# Pico-80

Exploratory work to build a Z-80 microcomputer that will behave like a 
TRS-80 Model I.  The idea is to use an actual Z-80 chip as the heart but
to replace the static RAM, ROM, and everything else, with a single stand-alone
modern microcontroller.  So all data to/from ROM, SRAM, etc, will be supplied
by the modern microcontroller.  This includes the clock signal for the Z-80.
No software emulation of the Z-80 CPU at all.

Initially the target is a working TRS-80 machine running unmodified TRS-80
software.  Later the design may be extended, as necessary, to behave as
other Z-80 machines of the era, like a ZX-Spectrum.  CP/M will be possible,
of course.  All that is required to behave like another machine is a change
in the modern microcontroller firmware.

The TRS-80 design implies:

* A 1.77MHz clock
* 16KB ROM
* 48KB RAM, including 1KB of video RAM 
* Keyboard
* A cassette port

All this will be supplied by the modern microcontroller.  This
will initially be a Raspberry Pi Pico.

The 1KB video RAM will be used by the Pico (initially) to produce HDMI (DVI)
or VGA video.  Or maybe there will be a dedicated LCD display.

The final TRS-80 machine will behave as if it has the video lowercase
modification installed.  Hopefully there will also be emulated floppy drives,
used to run NewDos-80.

## Genesis

The initial idea for this project came from this Hackaday article:

https://hackaday.com/2018/07/28/the-4-z80-single-board-computer-evolved/

Later, another article added fuel to the fire by describing a similar
approach to the article above:

https://www.hackster.io/james-fitzjohn/raspberry-pi-to-z80-interface-0bfbeb

## Initial Design(s)

First thing to do is assemble the Z80-MBC2 kit I have.  Get that working first.

Then simplify the IOS_Lite control code to see how the Z80 is driven.

Then test using a small 8 channel logic analyzer I have.

Next build on the circuit here:

https://hackaday.com/2021/02/09/interfacing-a-z80-cpu-with-the-raspberry-pi/

## Nomenclature

The Z-80 chip is the CPU.  The modern microcontroller that provides everything
else is called the IOS (I/O Subsystem). 
