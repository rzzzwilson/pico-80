This directory contains a version of the original ISO-Lite code
that has been reformatted to reduce line length.

The modified code produces an identical \*.hex file compared to
that produced by the original code, apart from differences in the
 __DATE__ and __TIME__ results.
